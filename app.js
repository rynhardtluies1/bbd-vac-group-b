// This is the main entry point of the central server application

//===== GLOBAL IMPORTS =====//
const path = require('path');
const bodyParser = require('body-parser');
const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
// const rootDir = require('./util/path');

//===== GLOBAL VARIABLES =====//
// Express endpoint server
const app = express();
const verbose = true;

// Socket.io server
const sockets = http.createServer(app);
const io = new Server(sockets, { cors: { origin: '*' } });

//===== PATH MANIPULATION =====//
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));



//========== EXPRESS API ENDPOINTS ==========//

// Page routes
const hostRoutes = require('./routes/host_routes.js');
const playerRoutes = require('./routes/player_routes.js');
app.use('/host', hostRoutes);
app.use('/player', playerRoutes);




//========== SOCKET CONNECTIONS ==========//

// All server-side socket handlers
const handlers = {
	"player": require('./sockets/playerWebAppHandler.js'),
	"host": require('./sockets/hostWebAppHandler.js')
};

// Maps a socket's id to its personal dispatch function
const dispatchers = {
	"player": {},
	"host": {}
}

// Dispatch an event to a client device
// * role: "player" | "host"
// * socketId: string | "all"
// * commandName: string
// * data: any
function dispatch(role, socketId, commandName, data) {
	if (!dispatchers[role]) {
		console.error("Invalid role:", role);
		return;
	}
	if (!dispatchers[role][socketId]) {
		if (socketId !== "all") {
			console.error("Invalid socketId:", socketId);
			return;
		}

		// Dispatch to all sockets of this role
		for (const id in dispatchers[role]) {
			if (!dispatchers[role].hasOwnProperty(id)) continue;
			dispatchers[role][id](commandName, data);
		}
		return;
	}

	dispatchers[role][socketId](commandName, data);
}

io.on('connection', (socket) => {
	console.log("SOCKET CONNECTED:", socket.id);

	//===== SOCKET CONFIG STUFF =====//

	socket.on('ehlo', (role) => {
		if (!handlers[role]) {
			console.error("Invalid role:", role);
			return;
		}

		// Start listening for receiveEvents
		for (const eventName in handlers[role].receiveEvents) {
		    if (!handlers[role].receiveEvents.hasOwnProperty(eventName)) continue;

			// E.g. player/playerJoined
		    socket.on(`${role}/${eventName}`, (data) => {
				if (verbose) console.log("Received event:", eventName, data);
		        handlers[role].receiveEvents[eventName](data);
		    });
		}

		for (const commandName in handlers[role].sendCommands) {
			if (!handlers[role].sendCommands.hasOwnProperty(commandName)) continue;

			dispatchers[role][socket.id] = (commandName, data) => {
				const payload = handlers[role].sendCommands[commandName](data);
				if (payload !== null) {
					socket.emit(commandName, payload);
				}
			}
		}
	});

	socket.on('disconnect', () => {
		console.log("SOCKET DISCONNECTED", socket.id);
		for (const role in dispatchers) {
			if (!dispatchers.hasOwnProperty(role)) continue;
			if (dispatchers[role][socket.id])
				delete dispatchers[role][socket.id];
		}
	});
});

//===== PORT ASSIGNMENT =====//
if (process.env.PORT == null) {
	sockets.listen(3000, () => {
		console.log('listening on port 3000');
	});
} else {
	sockets.listen(process.env.PORT, () => console.log(`Server is active on port: ${process.env.PORT}`));
}



//========== INITIALIZE GAME STATE ==========//
const { GameState } = require('./gamestate/state.js');
const gameState = new GameState(dispatch);

// TODO: Remove this, just for testing
setInterval(() => {
	dispatch("player", "all", "questionChanged", Math.random());
}, 1000);