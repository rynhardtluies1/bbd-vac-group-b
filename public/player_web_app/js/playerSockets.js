const socket = io("http://localhost:3000");

//========== HTML ELEMENTS ==========//
var questionText = null;
window.onload = () => {
    questionText = document.getElementById("questionText");
}

// Since we're not using typescript, please specify the data types
// you are sending in the params for each of these functions :)
// You can use pseudo-typescript syntax I guess
const sendCommands = {
    // * answerChosen: "A" | "B" | "C" | "D"
    // * returns: "A" | "B" | "C" | "D"
    "userLogin": ({ username, password }) => {
        if (username.length > 0 && password.length > 0) {
            return { username, password };
        }
        return null; //returning null does not send anything to the server
    },
    "userAnswered": (answerChosen) => {
        // The return value here is what will be sent to the server
        // These functions are where you can do any necessary preprocessing
        return answerChosen;
    },
    "playerJoined": (data) => {
        return data;
    }
};

// These are events that you want to listen for on the client mobile application
// Don't change this at runtime, all events should be defined at the start
const receiveEvents = {
    "questionChanged": (newQuestion) => {
        if (questionText)
        questionText.innerHTML = newQuestion;
    }
};


//===== SOCKET CONFIG STUFF =====//
// You can mostly ignore this

var socketId = null;

socket.on('connect', function() {
    console.log('Connected to the server');
    socket.emit("ehlo", "player"); // This tells the server that this is a player
    dispatch("playerJoined", { id: socketId });
});

socket.on('disconnect', function() {
    console.log('Disconnected from the server');
});

// Auto close the socket connection when the window is closed
window.onbeforeunload = function() {
    socket.disconnect();
    console.log('Socket disconnected due to page unload');
};

// This is the function that you call to dispatch one of the above sendCommands
function dispatch(commandName, data) {
    console.log("Sending event:", commandName, data);
    if (sendCommands[commandName]) {
        const payload = { socketId, data: sendCommands[commandName](data) };
        if (payload !== null) {
            socket.emit("player/" + commandName, payload);
        }
    }
}

// Start listening for receiveEvents
for (const eventName in receiveEvents) {
    if (!receiveEvents.hasOwnProperty(eventName)) continue;

    socket.on(eventName, (data) => {
        receiveEvents[eventName](data);
    });
}