class QuizState {
    constructor() {
        this.currentQuestion = 0;
    }

    getCurrentQuestion() {
        return questions[this.currentQuestion];
    }

    nextQuestion() {
        this.currentQuestion++;
        this.currentQuestion %= questions.length;
    }
}

const questions = [
    {
        question: "What is the square root of 4?",
        answers: [ "6", "9", "4", "2" ],
        correctAnswer: 3
    },
    {
        question: "What is the capital of France?",
        answers: [ "Paris", "London", "Berlin", "Madrid" ],
        correctAnswer: 0
    }
]

module.exports = { QuizState };