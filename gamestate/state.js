const crypto = require("crypto");
const { QuizState } = require("./quiz");

class GameState {

    // * dispatch: (socketId: string, commandName: string, data: any) => void
    constructor(dispatch) {
        this.teams = [];
        this.quizState = new QuizState();
        this.dispatch = dispatch;
    }

    // team: Team
    addTeam(team) {
        this.teams.push(team);
    }

    // * player: Player
    // * teamNo: number
    addPlayer(player, teamNo) {
        if (teamNo < 0 || teamNo >= this.teams.length) {
            console.error("Invalid team number:", teamNo);
            return;
        }

        this.teams[teamNo].addPlayer(player,);
    }
}

class Team {
    // * colour: TeamColour
    constructor(colour) {
        this.colour = colour;

        // `players` and `points` are both indexed by playerId
        // This way if a player loses connection we still have their points
        this.players = {};
        this.points = {};
    }

    // * teamNo: number
    // * returns: playerId
    createPlayer(teamNo) {
        const playerId = crypto.randomBytes(16).toString("hex");
        const newPlayer = new Player(username, teamNo);

        this.currentPlayers[playerId] = newPlayer;

        return playerId;
    }

    // Points are stored individually per player
    // * returns: number
    getTotalPoints() {
        let total = 0;
        for (const p in this.players) {
            total += p.points;
        }
        return total;
    }

    // * playerId: string
    // * points: number
    addPoints(playerId, points) {
        if (!this.points[playerId]) {
            this.points[playerId] = 0;
        }
        this.points[playerId] += points;
    }
}

class Player {
    // * playerId: string
    // * username: string
    // * team: number
    constructor(playerId, username, teamNo) {
        this.playerId = playerId;
        this.username = username;
        this.teamNo = teamNo
    }
}

// These should be CSS colour strings
const TeamColours = [
    "red",
    "blue",
    "green",
    "yellow"
];

module.exports = { GameState };